
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to describe squirrels in Central Park

## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:
J’ajoute une ligne ! J’ajoute une autre ligne

``` r
library(squirrels)
## basic example code
check_primary_color_is_ok("Gray")
#> [1] TRUE
```
